# -*- coding:utf-8 -*-

'''
MLP:
('Test accuracy:', 0.77121213782917375)
'''

import pre_process
import numpy as np
import pandas as pd
from keras.preprocessing.text import Tokenizer
from keras.utils import to_categorical
from keras.layers import Dense, Dropout
from keras.models import Sequential
from keras.models import load_model
import sys
reload(sys)

sys.setdefaultencoding('utf8')

def train():
    words, labels = pre_process.get_data()
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(words)
    sequences = tokenizer.texts_to_sequences(words)
    word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(word_index))
    data = tokenizer.sequences_to_matrix(sequences, mode='tfidf')
    labels = to_categorical(labels + 1)

    # 建立模型
    print('Building model...')
    model = Sequential()
    model.add(Dense(512, input_shape=(len(word_index) + 1,), activation='relu'))
    model.add(Dropout(0.3))
    model.add(Dense(labels.shape[1], activation='softmax'))
    model.summary()

    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

    batch_size = 32

    history = model.fit(data, labels,
                        batch_size=batch_size,
                        epochs=3,
                        verbose=1,
                        validation_split=0.1)
    pos = len(data)*0.9

    score = model.evaluate(data[int(pos):], labels[int(pos):],
                           batch_size=batch_size, verbose=1)
    print('Test score:', score[0])
    print('Test accuracy:', score[1])

    model.save('mlp.h5')

def predict():
    comment = np.loadtxt('./predict.txt', dtype=str)
    sentences = pd.Series(comment)
    sentences = sentences.apply(lambda s: pre_process.doSeg_cut(s))
    words, labels = pre_process.get_data()

    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(words)

    text_seq = tokenizer.texts_to_sequences(sentences)
    data_seq = tokenizer.sequences_to_matrix(text_seq, mode='tfidf')

    model = load_model('mlp.h5')
    predic = model.predict(data_seq)

    file = open("./predict_result.txt", "w")

    count = 0
    for i in predic.argmax(axis=1):
        print >> file, i-1,
        print >> file, sentences[count]
        count += 1

    file.close()

#train()
predict()