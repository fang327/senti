MLP情绪分类

训练原始数据保存在original_data.csv中
训练结果保存在train_result.txt中
如果需要重新训练，只需要替换掉original_data.csv中的数据，调用train()函数

预测文件是predict.txt,如果需要预测文本情绪，将文本写入predict.txt,一行一条数据，调用predict()函数，预测结果会保存在predict_result.txt文件中