# -*- coding:utf-8 -*-

'''
数据预处理
'''

import jieba
import re
import pandas as pd

def doSeg_cut(sentence):  #未切词状态
    try:
        sentence = sentence.decode("utf8")
        sentence = re.sub("[\s+\.\!\/_,$%^*(+\"\']+|[+——！，。？、~@#￥%……&*（）]+".decode("utf8"), "".decode("utf8"), sentence)
    except Exception , e:
        print sentence

    seg_lists = jieba.cut(sentence, HMM=True)

    stopwords = []
    for word in open("./stop_words.txt", "r"):
        stopwords.append(word.strip())

    ll = ''
    for seg in seg_lists :
        if (seg.encode("utf-8") not in stopwords and seg != ' ' and seg != '' and seg != "\n" and seg != "\n\n"):
            ll = ll + seg.encode("utf-8") + ' '
    return ll

def get_data():
    original_data = pd.read_csv('./original_data.csv', header=None)

    words = original_data[1][1:]
    labels = original_data[0][1:]
    labels = pd.to_numeric(labels).astype(int)

    return words, labels
